Note: a (v) next to a function name implies that function is private.

board.cpp/.h - This file contains a board 
                    constructor, 
                    destructor, 
    copy            copies boards,
    occupied (v)    detects whether a space is occupied,
    get (v)         detects whether or not a piece at x, y is black/white
    set (v)         assigns a color to a location x,y,
    onBoard (v)     checks whether or not a location, x,y, is on the board
    isDone          checks whether or not the game is finished
    hasMoves        checks whether a side has moves or not
    checkMove       checks if a move is legal
    doMove          modifies board to reflect a move
    count           counts the number of stones of a certain color on board
    countBlack      counts black stones on board
    countWhite      counts white stones on board
    getScore        score, with offset for corners and edges
    setBoard        sets the board to any position 
    tryAllMoves     creates a vector of all possible first moves on board
    alphabeta       runs a minimax tree with alpha-beta pruning
    and all of the multiplier constants
                    
player.cpp/.h - This file contains a player
                      constructor,
                      destructor,
    doMove            returns a pointer to a legal move to make
    doRandomMove      returns a pointer to the first legal move found
    doProfitableMove  returns a pointer to the move that leaves the board in
                      the most favorable state
    doABMove          follows all legal move sequences to a given depth and 
                      returns the move leading to the maximum minimal score.
                      found via minimax tree with alpha-beta pruning
    tryAllMoves       tries all legal moves on the current board
    makeFirstMove     finds and makes all legal moves on the current board
    minimaxTree       follows all legal move sequences to a given depth and 
                      returns the move leading to the maximum minimal score

Contributions:
Bella:
I worked on the valiant attempt of minimax tree (It never gave the right
answer), gave up on it, and then worked on alpha-beta pruning. I wrote
tryAllMoves, doABMove, minimaxTree (and helper functions that have
since been deleted), alphabeta, and timeElapsed, which Julie and I debugged.
I also worked on the README

Julie:
I wrote doRandomMove and doProfitableMove and worked on alpha-beta pruning.
The board scoring is my fault, for better or worse.
Did a fair amount of debugging and worked on the README.

Improvements:
Completed:
We think we successfully implemented alpha beta pruning, which will help us 
maximize our guaranteed minimum score. This works well against SimplePlayer,
but not so well against better player. We go down to a depth of 12, and always
finish with plenty of time to spare. With out pruning, we would not be able to 
look at as many levels of the tree.

Attempted:
We tried to do a bastardized version of iterative deepening, where we would
return as soon as we hit the alloted amount of time for each move. This didn't
work very well, since we would only ever explore a small part of the tree.
We also started attempts to do a different bastardized version of iterative
deepening, where we would start off with a small depth, and keep on going
further if we had extra time left alloted to a particular move. We realized
how redundant this would be, and stopped.































