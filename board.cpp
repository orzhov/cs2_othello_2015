#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 *
 * Each position is represented as an integer, n, such that n mod 8 = x , and
 * floor(n / 8) = y. Setting a piece down 
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::makeMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Given a color, calculates that color's score on the board
 * by subtracting the number of enemy pieces from the number of friendly
 * pieces and adding an offset for edges and corners. 
 */
int Board::getScore(Side side) {
  int score = 0;
  int multiplier;

  // Look at each square
  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < LENGTH; j++) {
      if (occupied(i, j)) {

        // Check for corner
        if (((i == 0) || (i == WIDTH - 1)) && ((j == 0) || (j == LENGTH - 1))) {
          multiplier = CORNER_MULT;
        }

        // Check for adjacent to corner
        else if (((i <= 1) || (i >= WIDTH - 2)) && \
                 ((j <= 1) || (j >= LENGTH - 2))) {
          multiplier = KITTY_MULT;
        }

        // Check for side
        else if (((i == 0) || (i == WIDTH - 1)) || \
                 ((j == 0) || (j == LENGTH - 1))) {
          multiplier = SIDE_MULT;
        }

        // Interior
        else {
          multiplier = INTER_MULT;
        }

        if (get(side, i, j)) {
          score += multiplier;
        }
        else {
          score -= multiplier;
        }
      }
    }
  }

  return score;

}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

//////////////////////////////////////////////////////////////////////////////
// Alpha-Beta Pruning Functions                                             //
//////////////////////////////////////////////////////////////////////////////

/*
 * Make all legal moves on
 * copies of the current game board, and returns an array of boards
 * with these moves made on them.
 */
vector<Board *> Board::tryAllMoves(Side color) {
  vector<Board *> allMoves;
  Move *test_move = new Move(OFF_BOARD, OFF_BOARD);

  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < LENGTH; j++) {
      // Iterate through all squares, setting next_move
      test_move->setX(i);
      test_move->setY(j);

      // Check for legality
      if (checkMove(test_move, color)) {
        // Copy the board
        Board *test_stage = copy();

        // Put the move on the copied board
        test_stage->makeMove(test_move, color);

        // We want to store the resulting board in a vector.
        allMoves.push_back(test_stage);
      }
    }
  }
  delete test_move;

  return allMoves;
}

/* ALPHA BETA PRUNING
 * 
 * Standard alpha beta pruning, with msLeft keeping track of how much
 * time is left before we have to return. The time alloted to alphabeta
 * the first time around is determined by doMove.
 *
 * a = our best overall score
 * b = our opponent's best overall score
 */

int Board::alphabeta(int depth, int a, int b, bool max_me, 
                     Side color, clock_t start_ab, int msLeft) 
{
// std::cerr << "alphabeta called, msLeft = " << msLeft << std::endl;
    int min_possible_score = KITTY_MULT * LENGTH * WIDTH;
    int max_possible_score = CORNER_MULT * LENGTH * WIDTH;
    
    msLeft -= clock() - start_ab;
    Side other = (color == BLACK) ? WHITE : BLACK;

    int v;

    vector<Board *> allBoards;    
    /*
    if (msLeft < 50)      // not enough time left => a forced return
    {
        std::cerr << "out of time! Depth = " << depth << std::endl;
        depth = 0;
    }*/

    /* If out of time or desired depth = 0 or no legal moves, 
     * return current board score */
    if (depth == 0 ||  !(hasMoves(max_me ? color : other)))
        return getScore(color);

    /* Make vector of all possible moves on the board */
    allBoards = tryAllMoves(color);

    if (max_me) 
    {
        v = min_possible_score;
        for (unsigned int i = 0; i < allBoards.size(); i++) 
        {
            Board *child = allBoards[i];
            v = max(v, child->alphabeta
                   (depth - 1, a, b, false, color, start_ab, msLeft));
            a = max(a, v);
            if (b < a) 
                break;
        }
        for (unsigned int i = 0; i < allBoards.size(); i++) 
        {
            delete allBoards[i];
        }   
        return v;
    }
   
    else 
    {
        v = max_possible_score; 
        for (unsigned int i = 0; i < allBoards.size(); i++) 
        {
            Board *child = allBoards[i];
            v = min(v, child->alphabeta
                   (depth - 1, a, b, true, color, start_ab, msLeft));
            b = min(b, v);
            if (b < a)
                break;
        }
        for (unsigned int i = 0; i < allBoards.size(); i++) 
        {
            delete allBoards[i];
        }
        return v;
    }
}
