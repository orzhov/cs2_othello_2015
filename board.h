#ifndef __BOARD_H__
#define __BOARD_H__

#include <vector>
#include <bitset>
#include "common.h"
#include <iostream>

#define LENGTH 8
#define WIDTH 8
#define OFF_BOARD -1

#define CORNER_MULT 3
#define SIDE_MULT 3
#define KITTY_MULT -3
#define INTER_MULT 1

using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void makeMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int getScore(Side side);
    void setBoard(char data[]);
    vector<Board *> tryAllMoves(Side color);
    int alphabeta(int depth, int a, int b, bool max_me, 
                  Side color, clock_t start_ab, int msLeft); 
};

#endif

