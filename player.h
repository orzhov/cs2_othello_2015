#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <vector>
#include <iostream>
#include "common.h"
#include "board.h"

using namespace std;

class Player {

private:
    Board *stage;
    Side color;
    Side opcol;
    Move *next_move;
    clock_t begin;
    int move_num;       // Uses number of stones on board to determine
                        // the move number.

public:
    Player(Side side);
    ~Player();
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *doRandomMove();
    Move *doProfitableMove(); 
    // vector<Player::Node*> makeFirstMove(Board testMe_board);   not needed
    // Move *minimaxTree(int depth); antiquated code
    Move* doABMove(int msLeft);
    Move* doIterativeABMove(int msLeft);
    double timeElapsed(clock_t sub_begin);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
