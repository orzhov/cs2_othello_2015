#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp. (So this wil always be set to
    // false)
    testingMinimax = false;
    stage = new Board();
    move_num = 0;
    next_move = NULL;
    begin = clock();

    color = side;

    if (color == WHITE) {
      opcol = BLACK;
    }
    else {
      opcol = WHITE;
    }

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete stage;
    delete next_move;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 * 
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
// std::cerr << "doMove called, msLeft = " << msLeft << std::endl;
    // Game destroys next_move each turn, so reinitialize it
    next_move = new Move(OFF_BOARD, OFF_BOARD);

    // Update board with opponent's move
    stage->makeMove(opponentsMove, opcol);

    // return doProfitableMove();
    move_num++;
    return doABMove(msLeft / (33 - move_num));
}

/*
 * Update next_move and stage with a random legal move and return it or NULL if
 * no legal move is found.
 */
Move *Player::doRandomMove() {
  // Make a random legal play
  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < LENGTH; j++) {
      // Iterate through all squares, setting next_move
      next_move->setX(i);
      next_move->setY(j);

      // Check for legality
      if (stage->checkMove(next_move, color)) {
        // Update board with move about to be returned
        stage->makeMove(next_move, color);

        return next_move;
      }
    }
  }

  // No legal moves
  return NULL;
}

/* Gives the elapsed time from the clock_t object, sub_begin
 */ 

double Player::timeElapsed(clock_t sub_begin)
{
  double elapsed_time;
  elapsed_time = clock() - sub_begin;
  return elapsed_time;
}

/*
 * Check all legal moves and choose the one that
 * puts the board immediately into the most favourable state,
 * as defined by the difference between the number of friendly pieces
 * and the number of enemy pieces, weighted toward corners and edges.
 */
Move *Player::doProfitableMove() {
  // Initialize best_score to be worse than the score after any play
  int best_score = KITTY_MULT * LENGTH * WIDTH;

  Move *potential_move = new Move(OFF_BOARD, OFF_BOARD);
  Board *test_stage;

  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < LENGTH; j++) {
      // Iterate through all squares, setting next_move
      potential_move->setX(i);
      potential_move->setY(j);

      // Check for legality
      if (stage->checkMove(potential_move, color)) {
        // Copy the board
        test_stage = stage->copy();
        // Try the move on the copied board
        test_stage->makeMove(potential_move, color);
        // Get the board's score after the move
        int potential_score = test_stage->getScore(color);

        // Check to see if the score is the best yet
        if (potential_score > best_score) {
          // Update best_score and next_move
          best_score = potential_score;

          next_move->setX(i);
          next_move->setY(j);
        }

        delete test_stage;
      }
    }
  }

  // Check that a legal move was found
  if (next_move->getX() == OFF_BOARD) {
    return NULL;
  }
  // Update board with move about to be returned
  stage->makeMove(next_move, color);

  delete potential_move;

  return next_move;
}

/* Handles making the first move. This deals with things like keeping
 * track of the scores corresponding to the first move. This does NOT
 * deal with how much time to allot to each move. That is assigned in
 * doMove. 
 */

Move *Player::doABMove(int msLeft) {
// std::cerr << "doABMove called, msLeft = " << msLeft << std::endl;
  Move *test_move = new Move(OFF_BOARD, OFF_BOARD);
  int min_score = KITTY_MULT * WIDTH * LENGTH;
  int max_score = CORNER_MULT * WIDTH * LENGTH;
  int curr_score;
  int curr_best = min_score;
  int depth = 12;
  clock_t start_ab = clock();

  for (int i = 0; i < WIDTH; i++) 
  {
    for (int j = 0; j < LENGTH; j++) 
    {
      // Iterate through all squares, setting next_move
      test_move->setX(i);
      test_move->setY(j);

      // Check for legality
      if (stage->checkMove(test_move, color)) 
      {
        // Copy the board
        Board *test_stage = stage->copy();
        // Put the move on the copied board
        test_stage->makeMove(test_move, color);

        curr_score = test_stage-> alphabeta
        (depth, min_score, max_score, true, color, start_ab, msLeft / 7);
// Magic number (7) courtesy of 
// Knowledge-Free and Learning-Based Methods in Intelligent Game Playing

        if (curr_score > curr_best)
        {
            next_move->setX(i);
            next_move->setY(j);   
        }
        delete test_stage;          
      }
    }
  }
  // Update board with move about to be returned
  stage->makeMove(next_move, color);

  return next_move;
}

/*
 * Goes first to a depth of 1 and chooses the highest-scored move.
 * If time allows, next considers the opponent's responses 
 * to each depth-1 move and chooses the d-1 move that maximizes the 
 * min score at d-2.
 * Again if time allows, similarly consider d-3 moves.
 */
Move *Player::doIterativeABMove(int msLeft) {
  /*
  Move *best_move;
  unsigned int depth = 1;
  int a = KITTY_MULT * LENGTH * WIDTH;
  int b = CORNER_MULT * LENGTH * WIDTH;

  while (true)
  {
    if (msLeft < 50)
      return NULL;

    stage->alphabeta(depth, a, b, true, color, msLeft);
    depth++;
  }
*/
  return NULL;

}

// ANTIQUATED CODE? (kept here for easy access purposes)

/*
 * Makes a tree of a specified depth, and calculates the scores of all the
 * resulting game states, returning the first move with the maximum minimal
 * gain for our side.
 *
 * int depth - refers to the number of moves made total, where we make 
 *             comes first, ending with an opponent move
 */ /*
Move *Player::minimaxTree(int depth) {
  int potential_score;
  Move *potential_move = new Move(OFF_BOARD, OFF_BOARD);
  Move *next_move = new Move(OFF_BOARD, OFF_BOARD);
  Board *test_stage;
  vector<Node*> curr_depth;
  vector<Node*> responses;
  vector<Node*> next_depth;
  vector<Node*> first_depth;
  Board curr_depth_board;
  int curr_depth_x;
  int curr_depth_y;
  vector<int> worst_score;
  int first_x;
  int first_y;
  int this_x;
  int this_y;
  int min_score = KITTY_MULT * LENGTH * WIDTH;

  // Make the first level of the tree. This level is special, because we need
  // to store the moves, so that we can have every board point to its
  // corresponding first move.
  test_stage = stage;
  first_depth = this->makeFirstMove(*test_stage);
  curr_depth = first_depth;

  // Make depth - 1 more levels of the tree
  for (int i = 0; i < depth - 1; i++) {
    for (unsigned int j = 0; j < curr_depth.size(); j++) {
      curr_depth_board = curr_depth[j]->b;
      curr_depth_x = curr_depth[j]->x;
      curr_depth_y = curr_depth[j]->y;
      responses = this->tryAllMoves(curr_depth_board, 
                                    curr_depth_x, curr_depth_y);
      next_depth.insert(next_depth.end(), 
                        responses.begin(), responses.end());
    }
    curr_depth.swap(next_depth);
    next_depth.clear();
  }

  for (unsigned int i = 0; i < first_depth.size(); i++){   
      // Initialize worst_score to be better than the score after any play
      worst_score.push_back(CORNER_MULT * LENGTH * WIDTH);
  }

  // Score each board in curr_depth, finding min score corresponding
  // with each possible first move
  for (unsigned int j = 0; j < first_depth.size(); j++) {
    this_x = first_depth[j]->x;
    this_y = first_depth[j]->y;
      for (unsigned int i = 0; i < curr_depth.size(); i++) {
        potential_score = curr_depth[i]->b.count(color) - 
                          curr_depth[i]->b.count(opcol);
        first_x = curr_depth[i]->x;
        first_y = curr_depth[i]->y;
        if ((first_x == this_x) && (first_y == this_y)) {
          if (potential_score < worst_score[j]) {
              // Update worst_score
              worst_score[j] = potential_score;
          }
        }
     }
  }
  
  // Find max score over the minimial scores per first move
  for (unsigned int j = 0; j < first_depth.size(); j++) {
    if (worst_score[j] > min_score) {
        min_score = worst_score[j];
        this_x = first_depth[j]->x;
        this_y = first_depth[j]->y;
    }
  }

  next_move->setX(this_x);
  next_move->setY(this_y);
  
  // Check that a legal move was found
  if (next_move->getX() == OFF_BOARD) {
    return NULL;
  }
  // Update board with move about to be returned
  stage->makeMove(next_move, color);

  delete potential_move;

  return next_move;
}*/
